package Electrodomestico;


public class Lavadora extends Electrodomestico{
    float carga;

    public Lavadora() {
        carga=5;
    }

    public Lavadora(float precio_base, float peso) {
        super(precio_base, peso);
        carga=5;
    }

    public Lavadora(float carga, float precio_base, float peso, String color, char consumo_energetico) {
        super(precio_base, peso, color, consumo_energetico);
        this.carga = carga;
    }

    public float getCarga() {
        return carga;
    }

    @Override
    public void precioFinal() {
        super.precioFinal(); 
        if (carga>30) {
            precio_base+=50;
        } 
    }

    public float getPrecio_base() {
        return precio_base;
    }

    public void setPrecio_base(float precio_base) {
        this.precio_base = precio_base;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public char getConsumo_energetico() {
        return consumo_energetico;
    }

    public void setConsumo_energetico(char consumo_energetico) {
        this.consumo_energetico = consumo_energetico;
    }

    public String[] getColores_disponibles() {
        return colores_disponibles;
    }

    public void setColores_disponibles(String[] colores_disponibles) {
        this.colores_disponibles = colores_disponibles;
    }

    public char[] getLetras_consumo() {
        return letras_consumo;
    }

    public void setLetras_consumo(char[] letras_consumo) {
        this.letras_consumo = letras_consumo;
    }


    @Override
    public String toString() {
       
        return"Precio: "+precio_base+" Peso: "+peso+ " Color: "+color+" Consumo: "+consumo_energetico+" Carga: "+carga ;
    } 
   
    
    
}
